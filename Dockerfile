FROM centos:latest

RUN yum -y install httpd php

RUN echo "IncludeOptional /*.conf" > /etc/httpd/conf.d/apache.conf

ENTRYPOINT [ "/usr/sbin/httpd", "-DFOREGROUND" ]

